<?php

namespace App\Http\Controllers;

use App\Customtoken;
use Exception;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    
    const MAX_DETAILS  = 3;
    const MIN_DETAILS  = 2;
    const LIMIT_SINGLE = 2;
    const LIMIT_DUO    = 3;
    

    public function index( Request $request) {
        $view          = [];
        $payloadValid  = 'true';
        $nodesRequired = ['price', 'plan_details', 'discount'];
        //$payload       = $request->method() == 'GET' ? $request->get('payload') : $request->post('payload');
        $payload = $request->input('payload');

        $payloadErrors  = [];

        // vars was render on view
        $view['planB']          = 'false';
        $view['price']          = '0';
        $view['price_discount'] = '0';
        $view['discount']       = '';
        $view['plan_details']   = '[]';
        $view['homeland']   = env('HOMELAND');
        $view['legal_text'] = json_encode( ['legal_text' => null ] );
        $view['payloadParam'] = [];

        try{
                
            if( is_null( $payload ) || $payload == "" ) {
                $payloadValid = 'false';
            }

            if( is_string( $payload ) && $this->is_base64($payload) ) {
                $payload = base64_decode( $payload );
            }

            if ( ! $this->json_validator($payload)) {
                $payload = null;
                $payloadValid ='false'; 
            }

            try {

                if( $request->method() === 'POST' ) {

                    $payloadValid = 'true';
                    $view['payloadParam'] = ( is_string( $payload ) ) ? json_encode( json_decode( $this->cleanText( $payload ) ) ) : json_encode( $payload );

                } else {
                    $payloadValid = 'true';
                    $view['payloadParam'] = json_encode(json_decode( $this->cleanText( $payload ) ) );
                }

                if( is_null( $view['payloadParam'] ) || $view['payloadParam'] == 'null' ) {
                    throw new Exception('invalid format');
                }

            }catch(\Exception $e){
                $payloadValid = 'false';
                $payloadErrors[] = 'Los parametros son invalidos o no vienen en el formato correcto';
            }


            if( $payload != null && $payloadValid != 'false' ) {
                $json = json_decode( $payload );

                $view['planB'] = ( isset( $json->type ) && $json->type == 'duo' ) ? 'true' : 'false';

                if( isset( $json->price ) ) {
                    $view['price'] = (int)$json->price;
                }

                if( isset( $json->discount ) ) {
                    $view['price_discount'] = $json->discount;
                }

                if( isset( $json->plan_details ) ) {
                    $view['plan_details'] = json_encode( $this->cleanDetails( $json->plan_details ) ) ;
                }

                if( isset( $json->legal_text ) ) {
                    $view['legal_text'] = json_encode( [ 'legal_text' => $json->legal_text ] );
                }
            }

            if( ! $this->checkNodesIfExistOnArray( $nodesRequired, $view ) || $payloadValid == 'false' ) {

                $payloadValid = 'false';
                // $payloadErrors[] = 'Los parametros son invalidos o no vienen en el formato correcto';

            } else {

                if( $this->isNullOrEmpty( $view['price_discount']  ) || (int)$view['price_discount'] <= 0 ) {
                    $payloadValid = 'false';
                    $payloadErrors[] = 'Falta el valor del descuento del plan en los parámetros';
                }

                if( $this->isNullOrEmpty( $view['price']  ) ) {
                    $payloadValid = 'false';
                    $payloadErrors[] = 'Falta el valor del precio del plan en los parámetros';
                }

                if( $this->isNullOrEmpty( $view['plan_details'] ) ) {
                    $payloadValid = 'false';
                    $payloadErrors[] = 'Falta el valor del descuento del plan en los parámetros';
                }

                if( (int)$view['price'] < (int)$view['price_discount'] ) {
                    $payloadValid = 'false'; 
                    $payloadErrors[] = 'El descuento es mayor que el precio';
                }

                if( ! $this->countDetailsCase( json_decode( $view['plan_details'] ), $json->type ) ){
                    $payloadValid = 'false';
                    // $payloadErrors[] = ( $json->type == 'single' ) ? sprintf('%s %s', $messageError, self::LIMIT_SINGLE) : sprintf('%s %s', $messageError, self::LIMIT_DUO) ;
                    $payloadErrors[] = ( $json->type != 'single' ) ? 'Falta el nombre del segundo plan móvil en los parámetros' : 'Falta quitar un plan móvil en los parametros';
                }
            }


            $view['payloadValid'] = $payloadValid;
            $view['key']          = $this->createCustomkey();
            $view['payloadErrors'] = json_encode( $payloadErrors );
            return View('home', $view);
        }catch(\TypeError $e) {
            $view = [];
            $view['payloadValid'] = 'false';
            $view['payloadParam'] = "";
            $view['planB'] = "false";
            $view['key'] = "";
            $view['price'] = "0";
            $view['price_discount'] = "0";
            $view['plan_details'] = '[]';
            $view['homeland'] = env('HOMELAND');
            $view['legal_text'] = json_encode( ['legal_text'=> null] );
            $view['payloadErrors'] = json_encode( ['display error'] );
            return View('home', $view); 
        }
        catch(\Exception $e) {
            $view = [];
            return View('home', $view); 
        }


    }

    private function cleanDetails( ?array $details ) {
        if( is_null( $details ) || empty( $details ) ) {
            return [];
        }

        $data = array_map( function( $item ) {
            return $this->cleanText( $item );
        }, $details );

        return $data;
    }

    private function cleanText( ?string $text ) {

        $text = strip_tags( $text );
        $text = preg_replace("/'/", "", $text);

        return $text; 

    }

    private function createCustomkey() {

        return Customtoken::getNewToken();

    }

    private function countDetailsCase( array $data, string $type ) {
        $total = count( $data );
        $limit = ( $type == 'single' ) ? self::LIMIT_SINGLE : self::LIMIT_DUO;

        return $total == $limit;
    }

    private function countDetails( array $data ) {

        $total = count( $data );
        return ( $total >= self::MIN_DETAILS && $total <= self::MAX_DETAILS  );

    }

    private function checkNodesIfExistOnArray( array $keys , array $array ) {

        $keyChecked = [];
        $keysFromData = [];


        if( empty( $array ) ) {
            return false;
        }

        // get keys from data
        foreach( $array as $key => $item ) {
            $keysFromData[] = $key;
        }


        // check keys required and compare from data , true of false depends if exists on keys of data 
        foreach( $keys as $item ) {
            $keyChecked[ $item ] = in_array( $item, $keysFromData );
        }

        $validate = array_map( function( $item ) {
            return $item;
        }, $keyChecked );

        return ! in_array( false, $validate ) && count( $validate ) == count( $keys );
        
    }

    private function isNullOrEmpty( $value ) {

        return is_null( $value ) || $value == '';

    }
        

    private function json_validator( $data = NULL ) {

        if ( ! empty( $data ) ) {
            @json_decode( $data );
            return ( json_last_error() === JSON_ERROR_NONE );
        }

        return false;

    }

    private function is_base64( ?string $string ) {

        if( $this->isNullOrEmpty( $string ) ) {
            return false;
        }

        return (bool) preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $string);
    }

    // get html from original header site movistar
    // @deprecated
    // private function _header() {
    //     $HEADER = file_get_contents($this->DOMAIN . '/inc_header_footer/header.php');
    //     $html = str_replace('/inc_header_footer/', $this->DOMAIN . '/inc_header_footer/', $HEADER);
    //     $html = str_replace('href="/base', 'href="' . $this->DOMAIN . '/base', $html);
    //     return $html;
    //
    //
    // }


    // get html from original footer site movistar
    // @deprecated
    // private function _footer() {
    //     $FOOTER = file_get_contents($this->DOMAIN . '/inc_header_footer/footer.php');
    //     $html = str_replace('/inc_header_footer/', $this->DOMAIN . '/inc_header_footer/', $FOOTER);
    //     $html = str_replace('href="/base', 'href="' . $this->DOMAIN . '/base', $html);
    //     //$html = str_replace('src="/base', 'href="' . $this->DOMAIN . '/base', $html);
    //     $html = str_replace('src="/base', 'href="/packfull/public/assets/', $html);
    //     $html = str_replace('href="/aviso-legal', 'href="' . $this->DOMAIN . '/aviso-legal', $html);
    //     $html = preg_replace('/href="\/([a-zA-Z_-]{3,})\S/','href="' . $this->DOMAIN . '/$1', $html);
    //     return $html;
    // }
}
