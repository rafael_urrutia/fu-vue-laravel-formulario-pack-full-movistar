<?php

namespace App\Http\Controllers\Backend;

set_time_limit(0);

use App\Http\Controllers\Controller;
use App\Http\Requests\GenerateFileReportRequet;
use App\Request as AppRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use League\Csv\Writer;

class DashboardController extends Controller
{

    const MODALITY_PORTABILITY_NAME = 'Portabilidad';
    const MODALITY_NEW_NAME         = 'Nuevo';
    const FORMAT_DATE               = 'd/m/Y H:i:s';

    public function index() {
        $view['maxDate'] = Carbon::now()->format('Y-m-d');
        return View('backend.dashboard', $view);
    }

    public function download(string $fileName) {
        try {
            return Storage::download( sprintf('public/csv/%s', $fileName ) );
        }catch(\Exception $e) {
            return response()->json( ['success' => false, 'message' => 'FILE NOT FOUND'], 200 );
        }
    }


    /**
     * Check if file is on server
     */
    public function checkDownload(Request $request) {
        $file = $request->input('file');

        $result = Storage::exists( sprintf( "public/csv/%s", $file ) );

        if( ! $result ) {
            return response()->json( [ 'success' => false, 'message' => 'File not found' ] );
        }

        return response()->json( [ 'success' => true, 'message' => 'OK' ] );
        
    }



    /**
     * Generate file .csv to download, each time this function call, all files .csv are destroy
     */
    public function generateFileReport(GenerateFileReportRequet $request) {

        $request->validated();// execute validation

        $result = AppRequest::whereBetween('created_at', [ 
                                                $request->input('initDate') . " 00:00:00", 
                                                $request->input('endDate') . " 23:59:50" ])
                                            ->get();

        if( ! $result ) {
            return response()->json( ['success' => false, 'message' => 'cant not create file'], 200 );
        }

        $nameFile = sprintf("%s_%s", $request->input('initDate'),$request->input('endDate'));

        $name = $this->buildCsvArray( $result , $nameFile );

        
        return response()->json(
            [
                'success'     => true,
                'message'     => 'ok',
                'file'        => str_replace('public/csv/', '', $name),
                'countResult' => count( $result )
            ]
        );

    }

    private function buildCsvArray( object $data, string $nameFile ) {

        $header = [
             'Correlativo',
             'Plan',
             'Modalidad',
             'Numero a portar',
             'Modalidad 2',
             'Numero a portar 2',
             'Detalle Plan',
             'Precio normal',
             'Precio promo',
             'Rut',
             'Nombres',
             'Apellidos',
             'Numero contacto',
             'Email',
             'Region',
             'Comuna',
             'Calle',
             'Numero',
             'Numero de depto o casa',
             'Datos adicionales',
             'Region chip',
             'Comuna chip',
             'Calle chip',
             'Numero chip',
             'Numero de depto o casa chip',
             'Datos adicionales chip',
             'Texto Legal',
             'Creado'
        ];


        $records = [];


        foreach( $data as $item ) {

            $records[] = [
                $item['id'],
                $this->processRow( $item['plan_name'] ),
                ( $item['is_phone_new'] ) ? self::MODALITY_NEW_NAME : self::MODALITY_PORTABILITY_NAME, 
                $item['portability_phone'],
                $this->_checkTypeModalityPortability( 'TYPE', $item['is_phone_new_plan_b'] ),
                $this->_checkTypeModalityPortability( 'PORTABILITY_NUMBER', $item['portability_phone_plan_b'] ),
                $this->processRow( $item['plan_details'] ),
                $this->processRow( $item['plan_price'] ),
                $this->processRow( ( $item['plan_price'] - $item['plan_price_on_discount']  )),
                $this->processRow( $item['run'] ), 
                $this->processRow( $item['name'] ), 
                $this->processRow( $item['lastname'] ),
                $this->processRow( $item['phone_contact'] ), 
                $this->processRow( $item['email'] ),
                $this->processRow( $item->communes->regions->name ), 
                $this->processRow( $item->communes->name ),
                $this->processRow( $item['street'] ), 
                $this->processRow( $item['number_street'] ), 
                $this->processRow( $item['apartment_number'] ), 
                $this->processRow( $item['aditional_data'] ), 
                ( $item['other_commune_id'] != null && $item['other_commune_id'] > 0 ) ? $this->processRow($item->otherCommunes->regions->name ) : '',
                ( $item['other_commune_id'] != null && $item['other_commune_id'] > 0 ) ? $this->processRow( $item->otherCommunes->name ) : '',
                $this->processRow( $item['other_street'] ),
                $this->processRow( $item['other_number_street'] ),
                $this->processRow( $item['other_apartment_number'] ),
                $this->processRow( $item['other_aditional_data'] ),
                $this->processRow( $item['legal_text'] ),
                $item['created_at']->format( self::FORMAT_DATE )
            ];

        }

        $csv = \League\Csv\Writer::createFromString();
        $csv->setDelimiter(';');
        $csv->setOutputBOM(Writer::BOM_UTF8);
        $csv->insertOne($header);
        $csv->insertAll($records);
        $csv->getInputBOM();

        $content = $csv->getContent();

        $fileName = $this->createFile( $content, $nameFile );

        return $fileName;

    }

    private function processRow( ?string $value ) {
       // return ( ! is_null( $value ) ) ? $this->remove_accents( $value ) : ""; 
       return ( ! is_null( $value ) ) ? ( $value ) : ""; 
    }

    private function createFile( string $content, string $nameFile ) {
        $this->destroyFiles();
        $nameFile = preg_replace('/\//','-', $nameFile);
        $namePath = sprintf('public/csv/%s_%s.csv', $nameFile, \Str::random(3));
        Storage::disk('local')->put( $namePath, $content );

        return $namePath;
    }

    private function destroyFiles() {
        $files = Storage::allFiles('public/csv');
        // unset($files[0]);// is .gitignore
        Storage::delete( $files );

        return true;
    }

    private function _checkTypeModalityPortability( $type, $value ) {
        
        $return = null;
        
        if( $type == 'PORTABILITY_NUMBER' ) {
            
            if( ! is_null( $value ) ) {
                $return = $value;

            }

        }

        if( $type == 'TYPE' ) {

            if( ! is_null( $value ) ) {
                $return = ( $value ) ? self::MODALITY_NEW_NAME : self::MODALITY_PORTABILITY_NAME;
            }

        }

        return $return;
        
    }

}
