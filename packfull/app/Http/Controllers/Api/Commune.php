<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Commune as CommuneModel;
use App\Http\Resources\Commune as CommuneCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Cache;

class Commune extends Controller
{
    const STORE_FILES = 'file';

    public function byRegion(string $acronym) {

        $cache_key = 'regions_' . $acronym ;

        $value = Cache::store( self::STORE_FILES )->get( $cache_key );

        if( $value ) {
            return $value;
        }

        $store = CommuneCollection::collection(
            CommuneModel::whereHas('regions', function(Builder $query) use($acronym) {
                $query->where( 'acronym', strtoupper($acronym) );
            })->get()
        );

        Cache::store( self::STORE_FILES )->put( $cache_key , $store, (600 * 6) * 48);//two days

        return $store;
    }

}
