<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FormStorageApiRequest;
use App\Request;

class Form extends Controller
{
    public function storage( FormStorageApiRequest $request ) {

        $request->validated();// execute validation

        $input = $this->processRequest( $request );

        $saved = Request::saveRegisterAndNotify( $input );

        $result = ( $saved ) 
            ? [ 'success' => true , 'message' => 'OK'] 
            : [ 'error' => true, 'message' => 'cant save register' ];

        return response()
                ->json( $result , 200)
                ->header('Content-Type', 'application/json');

    }

    private function processRequest( FormStorageApiRequest $request ) {

        $input = $request->all();

        $json = ($input['form_information']);
        $input['form_information'] = $json;

        // other direction
        $other = $input['other_direction'];
        unset($input['other_direction']);

        if( ! $input['use_current_street'] ) {
            $input['other_commune_id']       = $other['commune_id'];
            $input['other_street']           = $other['street'];
            $input['other_number_street']    = (int)$other['number_street'];
            $input['other_apartment_number'] = (int)$other['apartment_number'];
            $input['other_aditional_data']   = $other['aditional_information'];
            $input['use_current_street']     = '1';
        } else {
            $input['use_current_street'] = '0';
        }

        $input['plan_name'] = env('PLAN_NAME');

        $input['plan_details'] = join(' | ', $input['plan_details']);

        return $input;
    }



}
