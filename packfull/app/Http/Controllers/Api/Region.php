<?php

namespace App\Http\Controllers\Api;

use App\Customtoken;
use App\Http\Controllers\Controller;
use App\Region as RegionModel;
use App\Http\Resources\Region as RegionCollection;
use Illuminate\Support\Facades\Cache;

class Region extends Controller
{
    const STORE_FILES = 'file';

    public function index() {
        
        //cache
        $value = Cache::store( self::STORE_FILES )->get('regions');

        if( $value ) {
            return $value;
        }

        $store = RegionCollection::collection( RegionModel::all() );

        Cache::store( self::STORE_FILES )->put('regions', $store, (600 * 6) * 48);// two days

        return $store;
    }
}
