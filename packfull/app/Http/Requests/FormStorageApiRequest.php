<?php

namespace App\Http\Requests;

use App\Rules\validaRun;
use Illuminate\Foundation\Http\FormRequest;

class FormStorageApiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'run'                      => ['required', new validaRun] ,
            'portability_phone'        => 'max:999999999|integer|nullable',
            'is_phone_new'             => 'required',
            'portability_phone_plan_b' => 'max:999999999|integer|nullable',
            'is_phone_new_plan_b'      => 'nullable',
            'name'                     => 'required',
            'lastname'                 => 'required' ,
            'phone_contact'            => 'required|integer|max:999999999',
            'email'                    => 'required' ,
            'street'                   => 'required' ,
            'number_street'            => 'required' ,
            'apartment_number'         => 'nullable|regex:/[\w-]{0,}$/' ,
            'aditional_data'           => 'max:600|nullable' ,
            'form_information'         => 'required' ,
            'commune_id'               => 'required|exists:communes,id',
            'plan_price'               => 'required|integer',
            'plan_price_on_discount'   => 'required|integer',
            'plan_details'             => 'required|max:500',
            'legal_text'               => ''

        ];
    }
}
