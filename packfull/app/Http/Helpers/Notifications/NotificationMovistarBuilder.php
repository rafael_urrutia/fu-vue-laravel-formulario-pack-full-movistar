<?php
namespace App\Http\Helpers\Notifications;

use App\Http\Helpers\Notifications\Mailing\MailingMovistar;
use App\Http\Helpers\Notifications\Mailing\Request\MailingRequest;

class NotificationMovistarBuilder {
   
    public static function notificationByEmail( $data ) {
    
        $request = self::createRequest( $data );
        
        return ( new MailingMovistar )->send( $request );

    } 

    private static function createRequest( $data ) {

        $runProcess  = self::splitRun( $data['run'] );
        $detailsPlan = self::splitDetailsPlanFromFormInformation( $data['form_information'] );
        $price1To12  = $data['plan_price'] - $data['plan_price_on_discount'];
        $price13To18 = $data['plan_price'];

        $request = new MailingRequest;

        $request->name         = $data['name'];
        $request->lastname     = $data['lastname'];
        $request->detailA      = $detailsPlan[0];
        $request->detailB      = $detailsPlan[1];
        $request->detailC      = @$detailsPlan[2];// can be null or undefined
        $request->price1To12   = $price1To12;
        $request->price13To18  = $price13To18;
        $request->priceNormal  = $data['plan_price'];
        $request->discount     = $data['plan_price_on_discount'];
        $request->email        = $data['email'];
        $request->phoneContact = $data['phone_contact'];
        $request->street       = self::processAddress(
                                    $data['street'], 
                                    $data['number_street'], 
                                    $data['apartment_number'] 
        );
        $request->runWithFormat = $data['run'];

        $request->streetChip = $request->street;

        // street data from modal on frontend form
        if( isset( $data['other_street'] ) && ! is_null( $data['other_street'] ) ) {
            $request->streetChip = self::processAddress( 
                $data['other_street'], 
                $data['other_number_street'], 
                $data['other_apartment_number'] 
            ); 
        }

        $request->run = $runProcess['number']; 
        $request->dv = $runProcess['validator'];
        $request->legalText = $data['legal_text'];

        return $request;

    }


    private static function splitdetailsplanfromforminformation( $details ) {
        $json = json_decode( $details );
        return $json->plan_details;
    }

    private static function splitrun( $run ) {
        $run = preg_replace("/\./",'', $run);
        $split = explode('-', $run);
        return [ 'number' => $split[0], 'validator' => $split[1] ];
    }

    private static function processaddress( $address, $number, $apartment_number ) {
        $fulladress = sprintf('%s #%s', $address, $number ); 
        $fulladress = self::addapartment( $fulladress, $apartment_number );

        return $fulladress;
    }

    private static function addapartment( $street, $number ) {
        if( is_null( $number ) ) {
            return $street;
        }

        return sprintf('%s departamento #%s', $street, $number);

    }


}
