<?php

namespace App\Http\Helpers\Notifications\Mailing;

use App\Http\Helpers\Notifications\Mailing\Request\MailingRequest ;
use Illuminate\Support\Facades\Log;

class MailingMovistar {

    private $endpoint = '';

    public function __construct()
    {
       $this->endpoint = 'https://api.movistar.cl/EventNotificationManagement/V1/sendLegacyEvent?apikey=tHmhElytfTAyvLCcvySEBEuQKrOXtHIl';
    }

    public function send(MailingRequest $request) {
       return $this->_curl( $request ); 
    }

    private function _curl(MailingRequest $request) {

        try{
            $postdata = json_encode( $this->params( $request ) );
            $ch = curl_init($this->endpoint);

            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Authorization: Basic ZTE1NmIzNGItZTkzOS00MmVhLWIxNjctMzhhNzkwOTljZWYyOjU5YjUwOGJmLTJiZTQtNGNmYS05YTE4LWRkYjAxY2M2NDE1ZA'
            ]);

            $result = curl_exec($ch);

            curl_close($ch);

            $resultJSON = json_decode( $this->cleanResponse( $result ) );


            if( ! isset( $resultJSON->estado ) || (int)$resultJSON->estado->codigoEstado != 200 ) {
                Log::emergency( 'Cant send Email',array( json_encode( $resultJSON )) );
                return false;
            }

            return true;
        }catch(\Exception $e) {
            Log::emergency( $e ) ;
            return false;    
        }


    }

    private function cleanResponse( $result ) {

        $jsonClean = preg_replace('/\\n/','', $result);
        $jsonClean = preg_replace('/\\t/','', $jsonClean);
        $jsonClean = preg_replace('/ /','', $jsonClean);

        return $jsonClean;


    }

    private function params(MailingRequest $request) {

        $params = [];

        $params['triggeringSystem'] = 'MODE';
        $params['notificationType'] = '001938';
        $params['identification'] = [
            'identificationType' => 'RUT',
            'identificationNumber' => $request->run,
            'identificationCode' => $request->dv
        ];
            
        $params['preferredContactMedia'] = 'EMAIL';
        $params['email'] = [
            'eMailAddress' => $request->email
        ];

        $params['templateParameters'] = [
            'productOffering' => [
                'name' => $request->detailB, //nombre 2do detalle
                'description' => $request->detailA,// nombre primer detalle

            ],
            'geographicAddress' => [
                'streetName'  => $this->cleanDirection( $request->streetChip ),
                'fullAddress' => $this->cleanDirection( $request->street ),
            ],
            'individualName' => [
                'fullName' => sprintf('%s %s', $request->name, $request->lastname)// nombre completo
            ],
            'contactMedium' => [
                'name' => $request->name // solo nombre
            ],
            'customer' => [
                'msisdn' => $request->phoneContact// telefono contacto
            ],

        ];

        $params['templateParameters']['documentAbstract'] = [
            'documentID' => $request->runWithFormat
        ];

        // 3er producto
        $params['templateParameters']['product'] = [
            'description' => $request->detailC,// nombre 3er detalle
            'discount' => [
                'amount' => $request->discount// descuento web por 12 meses
            ]
        ];

        // mes 1 al 12
        $params['templateParameters']['paymentMethod'] = [
            'amount' => [
                'amount' => $request->price1To12
            ]
        ];


        // precio mes 13 al 18
        $params['templateParameters']['productOffering']['price'] = [
                'amount' => $request->price13To18
        ];

        // precio normal
        $params['templateParameters']['amount'] = [
                'amount' => $request->priceNormal
        ];

        $params['templateParameters']['message'] = $request->legalText;

        return $params;


    }

    private function cleanDirection( string $direction ) {
        return preg_replace('/,/',' ', $direction);
    }


}
