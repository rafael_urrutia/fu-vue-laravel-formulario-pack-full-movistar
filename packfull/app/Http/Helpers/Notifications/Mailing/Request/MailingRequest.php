<?php  
namespace App\Http\Helpers\Notifications\Mailing\Request;

class MailingRequest {
    public $run;
    public $dv;
    public $runWithFormat;
    public $email;
    public $street;
    public $streetChip;
    public $name;
    public $lastname;
    public $phoneContact;

    public $detailA;
    public $detailB;
    public $detailC;

    public $price1To12;
    public $price13To18;
    public $priceNormal;
    public $discount;
    public $legalText;

}
