<?php

namespace App\Http\Middleware;

use App\Customtoken;
use Closure;

class CustomTokenIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = ( $request->header('tokenAPP') != null ) 
            ? $request->header('tokenAPP') : $request->input('tokenAPP');

        if( $token == null || ! Customtoken::validateToken( $token ) ) {

            return response()->json(['status' => false, 'message' => 'invalid token'],  422);
        }

        return $next($request);
    }
}
