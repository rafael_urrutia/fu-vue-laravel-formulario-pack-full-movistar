<?php

namespace App\Listeners;

use App\Events\NotifyRegisterEvent;
use App\Http\Helpers\Notifications\NotificationMovistarBuilder;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class SendEmailNotificationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotifyRegisterEvent  $event
     * @return void
     */
    public function handle(NotifyRegisterEvent $event)
    {
        $data = $event->dataRequest;

        try {
            $sended = NotificationMovistarBuilder::notificationByEmail( $data );

            if( ! $sended ) {
                Log::error('Cant send email' , $data); 
            }
        }catch(\Exception $e) {
            Log::error($e);    
        }
    }
}
