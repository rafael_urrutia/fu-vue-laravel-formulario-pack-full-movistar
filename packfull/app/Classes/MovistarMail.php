<?php
/*
 *
 * DEPRECADA
 *
 *
 * */
namespace App\Classes;

class MovistarMail {
    
    
    private $URL_API_MOVISTAR            = "";
    private $ClIENT_SECRET_MOVISTAR      = "";
    private $ClIENT_ID_MOVISTAR          = "";
    private $URL_REDIRECT_MOVISTAR       = "";
    
    public function __construct() {
        
        $this->URL_API_MOVISTAR = env('MAIL_API_MOVISTAR');
        $this->CLIENT_SECRET_MOVISTAR = env('MAIL_API_SECRET');
        $this->CLIENT_ID_MOVISTAR = env('MAIL_API_CLIENT_ID');
        $this->URL_REDIRECT_MOVISTAR = env('CLIENT_API_REDIRECT');

    }


    public function emailClient( $item ) {
        return true;
        $to_email = $item['email'];
        $run      = $item['run'];

        $data = [];
        $login = $this->loginPartner();

        if ($login && isset($login->estado->codigoEstado) && $login->estado->codigoEstado == 200) {
            $item['access_token'] = $login->datos->access_token;
            return $this->sendNotification( $item );
        }else{
            return 'Error al enviar correo';
        }
    }


    
    public function loginPartner(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
                CURLOPT_URL => $this->URL_API_MOVISTAR.'oauth2/login-partner/loginCajetin?apikey=e6aeddd2-07c9-4fde-8c4c-e34b136761b0',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => 'client_secret='.$this->CLIENT_SECRET_MOVISTAR.'&client_id='.$this->CLIENT_ID_MOVISTAR.'&redirect_uri='.$this->URL_REDIRECT_MOVISTAR.'&grant_type=authorization_code',
                CURLOPT_HTTPHEADER => array(
                            'Content-Type: application/x-www-form-urlencoded'
                        ),
                ));

        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response);
    }






    public function sendNotification( $item ) {

        
        $run= explode('-',$item['run']);

            $curl = curl_init();
                    curl_setopt_array($curl, array(
                            CURLOPT_URL => $this->URL_API_MOVISTAR.'EventNotificationManagement/V1/sendLegacyEvent',
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => '',
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_POSTFIELDS =>'{
        "triggeringSystem": "MODE",
                    "notificationType": "001800",
                    "preferredContactMedia": "EMAIL",
                    "identification": {
                        "identificationNumber": "'.$run[0].'",
                        "identificationType": "RUT",
                        "identificationCode": "'.$run[1].'"
                    },
                    "email": {
                        "eMailAddress": "'.$item['email'].'"
                    },
                    "templateParameters": {
                        "customer": {
                                            "msisdn": ""
                                                                },
                        "individualName": {
                                            "fullName": ""
                                                                },
                        "geographicAddress": {
                                            "fullAddress": ""
                                                                },
                        "productOffering": {
                                            "price": {
                                                                    "amount": "0"
                                                                                            }
                        },
                    "emailContact": {
                                        "eMailAddress": "'.$item['email'].'"
                                                            },
                    "message": "Lun a Vie de 09:30 a 19:00 hrs."
                }
            }',
                CURLOPT_HTTPHEADER => array(
                            'Authorization: Bearer '.$item['access_token'].'',
                            'Content-Type: application/json'
                        ),
                        ));

                $response = curl_exec($curl);

                return json_decode($response);



    }
}
