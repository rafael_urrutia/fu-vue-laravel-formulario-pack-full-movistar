<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commune extends Model
{
    protected $table = 'communes';
    protected $fillable = [
        'name',
        'region_id'
    ];


    public function regions() {
        return $this->belongsTo(Region::class,'region_id','id');
    }

}
