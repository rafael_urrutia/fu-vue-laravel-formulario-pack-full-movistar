<?php

namespace App;

use App\Events\NotifyRegisterEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Request extends Model
{
    protected $table = 'requests';
    protected $fillable = [
        'run', 
        'portability_phone',
        'is_phone_new',
        'portability_phone_plan_b',
        'is_phone_new_plan_b',
        'name',
        'lastname' ,
        'phone_contact',
        'email' ,
        'street',
        'number_street',
        'apartment_number',
        'aditional_data',
        'form_information',
        'commune_id',
        'use_current_street',
        'other_commune_id' ,
        'other_street',
        'other_number_street',
        'other_apartment_number',
        'other_aditional_data',
        'plan_name',
        'plan_price',
        'plan_price_on_discount',
        'plan_details',
        'legal_text'
    ];

    public function communes() {
        return $this->belongsTo( Commune::class,'commune_id','id' );
    }

    public function otherCommunes() {
        return $this->belongsTo( Commune::class, 'other_commune_id', 'id' );
    }

    public static function saveRegisterAndNotify( $data ) {

        try {
            
            DB::beginTransaction();

            $item      = self::create( $data );
            $itemSaved = $item->save();

            if( ! $itemSaved ) {
                Log::emergency('cant save the register, execute rollback', $data);
                DB::rollBack();
                return false; 
            } 


            event( new NotifyRegisterEvent( $data ) );

            DB::commit();

            return true;
        }catch(\Exception $e) {
            Log::error( $e );
            DB::rollBack(); 
        }

        return false;

    }

}
