<?php
/*
 * codigo base https://dev.to/wilburpowery/easily-use-uuids-in-laravel-45be
 * */


namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Customtoken extends Model
{

    // protected $guarded = [];
    protected $fillable = [ 'token' ];

    // insert uuid has a primary key
    public static function boot() {
        parent::boot();
        
        static::creating( function( $model ) {

            if( ! $model->getKey() ) {
                $model->id = (string) Str::uuid();
            }

        } );
    }

    public function getIncrementing() {
        return false;
    }

    public function getKeyType() {
        return 'string';
    }

    public static function getNewToken() {
        self::cleanOldTokens();
        
        $token = (string) Str::uuid();

        $model = new Customtoken;
        $model->token = $token;
        $model->save();

        return $token;
    }

    public static function validateToken( string $token ) {

        $items = Customtoken::where('token', $token)
                            ->where( 'created_at', '>=', Carbon::now()->subMinutes(env('APP_TOKEN_MINUTES_EXPIRES'))->format('Y/m/d H:i:s') )
                            ->first();

        return $items != null;
    }

    private static function cleanOldTokens() {
        Customtoken::where( 'created_at', '<=', Carbon::now()->subMinute(env('APP_TOKEN_MINUTES_CLEAN'))->format('Y/m/d H:i:s') )
            ->delete();
    }



}
