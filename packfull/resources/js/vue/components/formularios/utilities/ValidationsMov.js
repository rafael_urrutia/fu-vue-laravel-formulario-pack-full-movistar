export class ValidationsMov {
    constructor(){}

    static required( value ) {
        try{
            return value != null && value != ''; 
        }catch(err) {
            console.log(value)
            return false
        }
        
    }

    static apartmentNumber( value ) {
        if ( value == null || value.trim() == '' ) {
            return true;
        }
        
        return /[\w-]{1,}$/.test( value );

    }

    static numeric( value ) {
        if(value == null || value.trim() == '') {
            return true;
        }

        return !isNaN(value) && 
                     parseInt(Number(value)) == value && 
                     !isNaN(parseInt(value, 10));

    }

    static email( value ) {
        if(value == null || value.trim() == '') {
            return true;
        }
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test( value );
    }

    static phone( value ) {
        return /^[0-9]/.test( value ) && value.length === 9;
    }

    static run( value ) {
        let rut = value;
        
        // validate format 11.111.111-1
        if( ! /^[0-9]{1,2}.[0-9]{3}.[0-9]{3}-[0-9kK]{1}$/.test( value ) ) {
            return false;
        }

        if (rut.toString().trim() != '' && rut.toString().indexOf('-') > 0) {
       
            rut = rut.replace('.','');
            rut = rut.replace('.','');

            var caracteres = new Array();
            var serie = new Array(2, 3, 4, 5, 6, 7);
            var dig = rut.toString().substr(rut.toString().length - 1, 1);
            rut = rut.toString().substr(0, rut.toString().length - 2);

            for (var i = 0; i < rut.length; i++) {
                caracteres[i] = parseInt(rut.charAt((rut.length - (i + 1))));
            }

            var sumatoria = 0;
            var k = 0;
            var resto = 0;

            for (var j = 0; j < caracteres.length; j++) {
                if (k == 6) {
                    k = 0;
                }
            sumatoria += parseInt(caracteres[j]) * parseInt(serie[k]);
            k++;
        }

            resto = sumatoria % 11;
            let dv = 11 - resto;

            if (dv == 10) {
                dv = "K";
            } else if (dv == 11) {
                dv = 0;
            }

            return ( dv.toString().trim().toUpperCase() == dig.toString().trim().toUpperCase() ); 
        } else {
            console.log('este es el error')
            return false;
        }
    }

}
