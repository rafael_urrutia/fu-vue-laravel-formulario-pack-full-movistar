require('./bootstrap');

import Vue from 'vue'
import Vuex from 'vuex'
import App from './vue/app'
import "@babel/polyfill"

Vue.use(Vuex)

Vue.component("modal", {
    template: "#modal-template"
});

axios.defaults.headers = {
    'tokenAPP': _TRANS
}


// form storage
const store = new Vuex.Store({
    state: {
        planFullForm: {
            form_information: null,
            run: null,
            portability_phone: null,
            is_phone_new: null,
            portability_phone_plan_b: null,
            is_phone_new_plan_b: null,
            name: null,
            lastname: null,
            phone_contact: null,
            email: null,
            region: null,
            commune: null,
            street: null,
            number_street: null,
            apartment_number: null,
            aditional_information: null,
            use_current_street: null,
            plan_price: null,
            plan_price_on_discount: null,
            plan_details: null,
            other_direction: {
                region: null,
                commune: null,
                street: null,
                number_street: null,
                apartment_number: null,
                aditional_information: null
            }
        }

    }
})

const app = new Vue({
    el: '#app',
    store,
    components: {
        App
    }
});
