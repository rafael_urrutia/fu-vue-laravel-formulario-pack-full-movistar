<!-- Header -->
<div id="mainmenu">
    <div class="mvx-top">
      <div class="mvx-container">
        <p>Personas</p>
        <a href="//ww2.movistar.cl/empresas/">Empresas</a>
        <a href="//ww2.movistar.cl/corporaciones/">Corporaciones</a>
        <a
          href="//ww2.movistar.cl/ofertas/convenios-corporativos/"
          class="convenios"
        >
          Convenios corporativos
        </a>
      </div>
    </div>
    <div class="mvx-bottom">
      <div class="mvx-container">
        <a
          class="mvx-bottom_logo"
          href="//ww2.movistar.cl/"
          title="Ir a movistar.cl"
        ></a>
        <a
          class="mvx-bottom_empresas opt-first"
          href="//ww2.movistar.cl/empresas/"
          data-fz="Optimize Ir a empresas"
        >
          Ir a empresas
        </a>
        <i role="button" class="mvx-mob_back mvs-back"></i>
        <div class="mvx-bottom_nav">
          <div class="mvx-bottom_mob">
            <i role="button" class="mvx-mob_back xjs-backclose"></i>
            <span class="mvx-bottom_more">
              <a
                href="//ww2.movistar.cl/empresas"
                class="opt-first"
                data-fz="Optimize Ir a empresas"
              >
                Ir a empresas
              </a>
              <button class="xjs-more" role="button"></button>
              <div>
                <a
                  href="//ww2.movistar.cl/empresas/"
                  class="opt-first"
                  data-fz="Optimize Ir a empresas"
                >
                  Empresas
                </a>
                <a
                  href="//ww2.movistar.cl/corporaciones/"
                  class="opt-second"
                  data-fz="Optimize otros"
                >
                  Corporaciones
                </a>
              </div>
            </span>
          </div>
          <nav class="mvx-bottom_innernav">
            <a
              href="//catalogo.movistar.cl/equipomasplan/catalogo.html"
              data-content=".menu-equipos"
            >
              Equipos
            </a>
            <a href="//ww2.movistar.cl/movil/" data-content=".menu-movil">
              Móvil
            </a>
            <a href="//ww2.movistar.cl/hogar/" data-content=".menu-hogar">
              Hogar
            </a>
            <a
              href="//ww2.movistar.cl/servicios-digitales/"
              data-content=".menu-digital"
            >
              Digital
            </a>
            <a
              href="//atencionalcliente.movistar.cl/"
              data-content=".menu-ayuda"
            >
              Ayuda
            </a>
            <a class="club" href="//ww2.movistar.cl/club/">Club</a>

            <span>
              <a
                href="#"
                class="mvxnotificaciones"
                data-content=".menu-notificaciones"
              >
                Notificaciones
              </a>
              <a
                href="//ww2.movistar.cl/ofertas/"
                class="mvxofertas"
                data-content=".menu-ofertas"
              >
                Ofertas Web
              </a>
              <a
                href="//acceso.movistar.cl/SSO_AU_WEB/loginAction.do"
                class="mimovistar"
                data-content=".menu-mimovistar"
              >
                Mi Movistar
              </a>
            </span>
          </nav>
        </div>
        <label
          class="mvx-bottom_buscador trigger-buscador opt-second"
          data-fz="Optimize otros"
        >
          <button>Buscar</button>
          <input
            type="text"
            data-hj-whitelist=""
            class="srx-search mvxmobile"
            autocapitalize="off"
            autocorrect="off"
            autocomplete="off"
            tabindex="1"
            placeholder="Buscar en Movistar.cl"
          />
        </label>
        <div class="mvx-bottom_buttons">
          <a
            href="//ww2.movistar.cl/ofertas/"
            class="btn-toogle mvxofertas"
            data-content=".menu-ofertas"
          >
            Ofertas
          </a>
          <a
            href="//acceso.movistar.cl/SSO_AU_WEB/loginAction.do"
            class="btn-toogle mimovistar"
            data-content=".menu-mimovistar"
          >
            Mi Movistar
          </a>
          <a
            href="#"
            class="mvxbtn mvxnotificaciones"
            title="Notificaciones"
            data-content=".menu-notificaciones"
          >
          </a>
          <a
            href="//ww2.movistar.cl/empresas/"
            class="mvxbtn mvxdetalle"
            title="Movistar Empresas &amp; Corporaciones"
            data-content=".menu-empresas"
          >
            Empresas
          </a>
          <i
            role="button"
            class="xjs-mobmenu"
            title="Despliegue menu mobile"
          ></i>
        </div>
      </div>
    </div>
  </div>
  <div id="maincontent" class="mvx-alltabs">
    <div class="mvx-content_container">
      <div class="mvx-content menu-equipos">
        <div class="mvx-content_top">
          <button class="mvx-mob_back xjs-selfclose"></button>
          <p>Equipos</p>
        </div>
        <nav>
          <a
            href="https://catalogo.movistar.cl/equipomasplan/catalogo.html?_ga=2.215544531.593891313.1602004405-2136215024.1596723997"
          >
            Celulares con plan portabilidad<span>Ofertas</span>
            <small>Contrata tu equipo en cuotas sin interés</small>
          </a>
          <a
            href="//catalogo.movistar.cl/equipomasplan/catalogo.html?modalidad=2"
          >
            Renueva tu celular
            <small>Sigue disfrutando los beneficios de ser Movistar</small>
          </a>
          <a href="//ww2.movistar.cl/equipos/movistarone/">
            Smartphones con Movistar One
            <small>Obtén el último celular cada 12 meses</small>
          </a>
          <a
            href="//catalogo.movistar.cl/fullprice/catalogo/smartphones-liberados.html"
          >
            Celulares Liberados
            <small>Equipos sin plan con grandes descuentos</small>
          </a>
          <a href="//catalogo.movistar.cl/fullprice/catalogo/outlet.html">
            Outlet de Equipos Seminuevos
          </a>
          <a href="//catalogo.movistar.cl/fullprice/catalogo/accesorios.html">
            Accesorios
          </a>
          <a
            href="//catalogo.movistar.cl/fullprice/catalogo/casa-conectada.html"
          >
            Smart Home<small>Convierte tu casa en una inteligente</small>
          </a>
          <a href="/hogar/wifi-movil/#equipos-mifi-routers">
            Routers y MiFi Móvil
          </a>
          <section class="mvx-content_marcas xmarcas">
            <p>¿Fanático de tu marca?</p>
            <a
              href="//ww2.movistar.cl/equipos/apple/"
              class="mvx-iphone"
              title="Ir a equipos iPhone"
            ></a>
            <a
              href="//ww2.movistar.cl/equipos/huawei/"
              class="mvx-huawei"
              title="Ir a equipos Huawei"
            ></a>
            <a
              href="//ww2.movistar.cl/equipos/samsung/"
              class="mvx-samsung"
              title="Ir a equipos Samsung"
            ></a>
            <a
              href="//ww2.movistar.cl/equipos/motorola/"
              class="mvx-motorola"
              title="Ir a equipos Motorola"
            ></a>
            <a
              href="//ww2.movistar.cl/equipos/xiaomi/"
              class="mvx-xiaomi"
              title="Ir a equipos Xiaomi"
            ></a>
            <a
              href="//ww2.movistar.cl/equipos/lg/"
              class="mvx-lg"
              title="Ir a equipos LG"
            ></a>
          </section>
        </nav>
      </div>
      <div class="mvx-content menu-movil">
        <div class="mvx-content_top">
          <button class="mvx-mob_back xjs-selfclose"></button>
          <p>Móvil</p>
        </div>
        <nav>
          <a href="//ww2.movistar.cl/movil/planes-portabilidad/">
            Planes Portabilidad <span>Ofertas</span>
            <small>Cámbiate y mantén tu número u obtén uno nuevo</small>
          </a>
          <a href="//ww2.movistar.cl/movil/planes-2x1/">
            Planes 2x1
            <small>Pórtate y lleva dos planes al precio de uno</small>
          </a>
          <a href="//ww2.movistar.cl/movil/planes-renovacion/">
            Porta una línea adicional <span>Ofertas</span>
            <small> Si ya eres cliente móvil</small>
          </a>
          <a href="//ww2.movistar.cl/hogar/wifi-movil/">
            Internet WiFi Móvil
          </a>
          <a href="//ww2.movistar.cl/movil/salta-de-prepago-a-plan/">
            Salta de Prepago a Plan
          </a>
          <a href="//ww2.movistar.cl/movil/prepago/">Prepago</a>
          <a href="//ww2.movistar.cl/movil/bolsas/">Bolsas</a>
          <a
            href="//ww2.movistar.cl/movil/roaming/datos-ilimitados-no-clientes/"
          >
            Roaming
          </a>
          <a href="//ww2.movistar.cl/movil/mas-servicios/">
            Potencia tu Servicio Móvil
          </a>
        </nav>
      </div>
      <div class="mvx-content menu-hogar">
        <div class="mvx-content_top">
          <button class="mvx-mob_back xjs-selfclose"></button>
          <p>Hogar</p>
        </div>
        <nav>
          <a href="//ww2.movistar.cl/hogar/internet-hogar/">
            Internet Hogar <span>25% dcto</span>
            <small>El internet hogar más rápido de Chile</small>
          </a>
          <a href="//ww2.movistar.cl/hogar/pack-digital-netflix/">
            Pack Digital <span>Nuevo</span>
            <small>Internet + Netflix y TV Online</small>
          </a>
          <a href="//ww2.movistar.cl/hogar/pack-duos-internet-television/">
            Pack Dúos: Internet + Televisión
            <small>Nueva Movistar TV Todo en uno</small>
          </a>
          <a href="//ww2.movistar.cl/hogar/pack-duos-internet-telefonia/">
            Pack Dúos: Internet + Telefonía
          </a>
          <a href="//ww2.movistar.cl/hogar/pack-trios/">
            Pack Tríos <small>Internet + Telefonía + Televisión</small>
          </a>
          <a href="//ww2.movistar.cl/hogar/internet-fibra-optica/">
            Fibra Óptica
          </a>
          <a href="//ww2.movistar.cl/hogar/mas-servicios/">
            Mejora tu Servicio Hogar
          </a>
          <a href="//ww2.movistar.cl/hogar/television/">Televisión</a>
          <a href="//ww2.movistar.cl/hogar/telefonia/">Telefonía Hogar</a>
        </nav>
      </div>
      <div class="mvx-content menu-digital">
        <div class="mvx-content_top">
          <button class="mvx-mob_back xjs-selfclose"></button>
          <p>Digital</p>
        </div>
        <nav>
          <a href="//ww2.movistar.cl/servicios-digitales/movistar-play/">
            Movistar Play <small>Televisión donde y cuando quieras</small>
          </a>
          <a
            href="//ww2.movistar.cl/servicios-digitales/movistar-play/amazon-prime-video/"
          >
            Amazon Prime Video
            <small>Con cargo a tu boleta Movistar</small>
          </a>
          <a
            href="//ww2.movistar.cl/servicios-digitales/mcafee-seguridad-total/"
          >
            Seguridad Total McAfee
            <small>Controla, recupera y protege tus datos</small>
          </a>
          <a href="//ww2.movistar.cl/servicios-digitales/movistar-musica/">
            Movistar Música <small>Las mejores playlists de música</small>
          </a>
          <a href="//ww2.movistar.cl/servicios-digitales/modo-gaming/">
            Modo Gaming
            <small>Todo sobre videojuegos, en un solo lugar</small>
          </a>
          <a href="//ww2.movistar.cl/app-mi-movistar/">
            App Mi Movistar
            <small>Todo lo que necesitas, en la palma de tu mano</small>
          </a>
          <a
            href="//ww2.movistar.cl/servicios-digitales/paga-con-tu-boleta-o-saldo/"
          >
            Paga con tu boleta o saldo
          </a>
          <a href="//ww2.movistar.cl/servicios-digitales/movistar-cloud/">
            Movistar Cloud
          </a>
          <a
            href="//ww2.movistar.cl/servicios-digitales/asistencia-movistar/"
          >
            Asistencia Movistar
          </a>
          <a href="//ww2.movistar.cl/servicios-digitales/">
            Ver todos los servicios
          </a>
        </nav>
      </div>
      <div class="mvx-content menu-ayuda">
        <div class="mvx-content_top">
          <button class="mvx-mob_back xjs-selfclose"></button>
          <p>Ayuda</p>
        </div>
        <nav>
          <a href="//pagos.movistar.cl/publico">
            Paga tu cuenta Hogar o Móvil
          </a>
          <a href="//ww2.movistar.cl/recarga/">Recarga tu saldo</a>
          <a href="//ww2.movistar.cl/atencion-al-cliente/asistente-digital/">
            Asistente Virtual <small>Te ayuda a resolver tus dudas</small>
          </a>
          <a href="//atencionalcliente.movistar.cl/">
            Preguntas frecuentes
            <small>Móvil, Hogar, Internet, Televisión y más.</small>
          </a>
          <a href="//soporteequipos.movistar.cl/">
            Guías interactivas de smartphones
          </a>
          <a
            href="//ww2.movistar.cl/atencion-al-cliente/internet-hogar/fibra/"
          >
            Mundo Fibra <small>Conoce el futuro de la comunicación</small>
          </a>
          <a href="//catalogo.movistar.cl/seguimiento-despacho/">
            Seguimiento de despacho móvil
          </a>
          <a href="//www.movistar.cl/seguimiento-de-solicitud-servicio-fijo/">
            Estado de instalación o reparación hogar
          </a>
          <a href="//www.movistar.cl/setec/">
            Estado de servicio técnico móvil
          </a>
          <a
            href="//www.movistar.cl/web/movistar/atencion-al-cliente/pago-deducible-en-linea/"
          >
            Paga tu deducible de seguro
          </a>
          <a href="//ww2.movistar.cl/movistar-accesible/">
            Movistar Accesible
          </a>
        </nav>
      </div>
      <div class="mvx-content menu-empresas">
        <p class="mvx-content_poptitle">
          Soluciones pensadas para tu negocio
        </p>
        <a class="corp-empresas" href="//ww2.movistar.cl/empresas/">
          Movistar <em>Empresas</em>
        </a>
        <a class="corp-corporaciones" href="//ww2.movistar.cl/corporaciones/">
          Movistar <em>Corporaciones</em>
        </a>
      </div>
      <div class="mvx-content menu-notificaciones">
        <div class="mvx-content_top">
          <button class="mvx-mob_back xjs-selfclose"></button>
          <p>Notificaciones</p>
        </div>
        <p class="mvx-content_poptitle">Notificaciones</p>
        <section id="mvxnotificaciones"><h4>Cargando...</h4></section>
      </div>
      <div class="mvx-content menu-ofertas">
        <div class="mvx-content_top">
          <button class="mvx-mob_back xjs-selfclose"></button>
          <p>Ofertas</p>
        </div>
        <nav>
          <a href="//ww2.movistar.cl/ofertas/">
            Para contratar un nuevo servicio
          </a>
          <a href="//ww2.movistar.cl/ofertas/clientes/">
            Para mejorar tu servicio Movistar
          </a>
          <a href="//ww2.movistar.cl/ofertas/convenios-corporativos/">
            Ofertas para convenios corporativos
          </a>
        </nav>
      </div>
      <div class="mvx-content menu-mimovistar">
        <div class="mvx-content_top">
          <button class="mvx-mob_back xjs-selfclose"></button>
          <p>Mi Movistar</p>
        </div>
        <a
          class="mvxlogin"
          href="//acceso.movistar.cl/SSO_AU_WEB/loginAction.do"
        >
          Ingresa a Mi Movistar / Sucursal Virtual
        </a>
        <section class="mvxlogincontainer">
          <p class="mvxnonregister">¿No estás registrado?</p>
          <a class="link-movil" href="//pagos.movistar.cl/publico">
            Paga tu cuenta Hogar o Móvil
          </a>
          <a class="link-recarga" href="//ww2.movistar.cl/recarga/">
            Recarga tu celular o BAM
          </a>
          <a
            class="link-registro"
            href="//acceso.movistar.cl/SSO_AU_WEB/registroRutAction.do"
          >
            Registrate en Mi Movistar
          </a>
        </section>
      </div>
    </div>
  </div>
  <!-- Fin Header -->