
<section id="mainbreadcrumb">
    <nav>
      <a href="/"><i></i>Inicio</a>
    </nav>
  </section>

<!-- footer -->
<footer id="mainfooter">
    <div class="mvf-footer">
      <div class="mvf-footer_col">
        <h3 role="button" class="js-tooglef">Para los que somos Movistar</h3>
        <ul>
          <li>
            <a
              href="//catalogo.movistar.cl/equipomasplan/catalogo.html?modalidad=2"
            >
              Renueva tu celular
            </a>
          </li>
          <li><a href="//pagos.movistar.cl/publico">Paga tu cuenta</a></li>
          <li><a href="//ww2.movistar.cl/recarga/">Recarga tu celular</a></li>
          <li>
            <a href="//ww2.movistar.cl/movil/planes-renovacion/">
              Porta una línea adicional
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/hogar/mas-servicios/">
              Potencia tu Servicio Hogar
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/hogar/television/canales-premium/">
              Canales Premium
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/servicios-digitales/movistar-play/">
              Activa Movistar Play
            </a>
          </li>
          <li>
            <a
              href="//ww2.movistar.cl/servicios-digitales/movistar-play/#streaming"
            >
              Amazon Prime Video y Netflix en tu boleta
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/larga-distancia/">
              Telefonía Larga Distancia
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/app-mi-movistar/">
              Descarga nuestras aplicaciones
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/sucursales/">
              Sucursales y lugares de pago
            </a>
          </li>
          <li>
            <a
              href="//ww2.movistar.cl/atencion-al-cliente/opciones-de-contacto/"
            >
              Teléfonos y otras opciones de contacto
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/concursos-y-promociones/">
              Concursos y promociones
            </a>
          </li>
          <li>
            <a
              href="//ww2.movistar.cl/atencion-al-cliente/comunicados/como-ingresar-un-reclamo/"
            >
              Ingresa reclamo
            </a>
          </li>
        </ul>
      </div>
      <div class="mvf-footer_col">
        <h3 role="button" class="js-tooglef">¿Aún no eres Movistar?</h3>
        <ul>
          <li>
            <a
              href="//ww2.movistar.cl/hogar/internet-fibra-optica/?factibilizador"
            >
              Consulta Factibilidad Técnica Fibra
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/hogar/internet-hogar/">
              Planes Internet Hogar
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/hogar/pack-digital-netflix/">
              Pack Digital
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/hogar/pack-duos-internet-television/">
              Packs Dúos: Internet + TV
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/hogar/pack-trios/">
              Packs Tríos: Internet + TV + Telefonía
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/hogar/internet-fibra-optica/">
              Planes Internet Fibra Óptica
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/movil/planes-portabilidad/">
              Pórtate con tu número a Movistar
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/hogar/wifi-movil/">
              Internet WiFi Móvil
            </a>
          </li>
          <li><a href="//ww2.movistar.cl/movil/prepago/">Prepago</a></li>
          <li>
            <a
              href="//ww2.movistar.cl/movil/roaming/datos-ilimitados-no-clientes/"
            >
              Roaming
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/servicios-digitales/">
              Servicios Digitales
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/ofertas/">Ofertas Movistar.cl</a>
          </li>
          <li><a href="//ww2.movistar.cl/blog/">Blog Movistar</a></li>
          <li><br /></li>
          <li>
            <a href="//ww2.movistar.cl/empresas/" rel="nofollow">
              Portal Empresas
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/corporaciones/" rel="nofollow">
              Portal Corporaciones
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/mayoristas/" rel="nofollow">
              Portal Mayoristas
            </a>
          </li>
        </ul>
      </div>
      <div class="mvf-footer_col">
        <h3 role="button" class="js-tooglef">¿Buscas un nuevo celular?</h3>
        <ul>
          <li><a href="//ww2.movistar.cl/equipos/apple/">iPhone</a></li>
          <li>
            <a href="//ww2.movistar.cl/equipos/huawei/">Celulares Huawei</a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/equipos/samsung/">Celulares Samsung</a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/equipos/xiaomi/">Celulares Xiaomi</a>
          </li>
          <li><a href="//ww2.movistar.cl/equipos/lg/">Celulares LG</a></li>
          <li>
            <a href="//ww2.movistar.cl/equipos/motorola/">
              Celulares Motorola
            </a>
          </li>
          <li>
            <a href="//catalogo.movistar.cl/fullprice/catalogo/outlet.html">
              Outlet de celulares seminuevos
            </a>
          </li>
          <li>
            <a
              href="//catalogo.movistar.cl/equipomasplan/apple-iphone-11-64-gb.html"
            >
              iPhone 11
            </a>
          </li>
          <li>
            <a
              href="//catalogo.movistar.cl/equipomasplan/apple-iphone-se-64gb.html"
            >
              iPhone SE 2020
            </a>
          </li>
          <li>
            <a
              href="//catalogo.movistar.cl/equipomasplan/apple-iphone-xr-64gb.html"
            >
              iPhone XR
            </a>
          </li>
          <li>
            <a href="//catalogo.movistar.cl/equipomasplan/huawei-p40.html">
              Huawei P40
            </a>
          </li>
          <li>
            <a href="//catalogo.movistar.cl/equipomasplan/huawei-y8s.html">
              Huawei Y8S
            </a>
          </li>
          <li>
            <a
              href="//catalogo.movistar.cl/equipomasplan/samsung-galaxy-note20.html"
            >
              Samsung Galaxy Note 20
            </a>
          </li>
          <li>
            <a
              href="//catalogo.movistar.cl/equipomasplan/samsung-galaxy-a71-128gb-black.html"
            >
              Samsung A71
            </a>
          </li>
          <li>
            <a href="//catalogo.movistar.cl/equipomasplan/catalogo.html">
              Ver todo el catálogo
            </a>
          </li>
        </ul>
      </div>
      <div class="mvf-footer_col">
        <h3 role="button" class="js-tooglef">
          Regulaciones y enlaces de interés
        </h3>
        <ul>
          <li>
            <a href="//ww2.movistar.cl/hogar/test-velocidad/">
              Test de velocidad
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/atencion-al-cliente/comunicados/">
              Comunicados
            </a>
          </li>
          <li>
            <a
              href="//ww2.movistar.cl/atencion-al-cliente/comunicados/derechos-y-deberes-de-los-usuarios/"
            >
              Derechos y deberes de los usuarios
            </a>
          </li>
          <li>
            <a
              href="//ww2.movistar.cl/atencion-al-cliente/comunicados/reglamento-de-reclamos/"
            >
              Reglamento de reclamos
            </a>
          </li>
          <li>
            <a
              href="//ww2.movistar.cl/centro-de-ayuda/documentos/ReglamentoServiciosTelecomunicacionesDecreto18.pdf"
              rel="nofollow"
            >
              Reglamento de telecomunicaciones
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/sistema-de-alerta-de-emergencias/">
              Sistema de alerta de emergencias (SAE)
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/neutralidad-en-la-red/">
              Neutralidad en la red
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/cobertura-movil/">
              Cobertura Móvil 4G, 3G y 2G
            </a>
          </li>
          <li>
            <a
              href="//www.subtel.gob.cl/estudios/indicadores-de-calidad-de-red-movil/"
              rel="nofollow"
            >
              Indicadores Calidad de Red Móvil de subtel
            </a>
          </li>
          <li>
            <a
              href="http://antenas.subtel.cl/LeyDeTorres/inicio"
              rel="nofollow"
            >
              Regulación de Antenas
            </a>
          </li>
          <li>
            <a href="//www.movistar.cl/desbloqueo-de-equipos-moviles">
              Desbloqueo de equipos móviles
            </a>
          </li>
          <li>
            <a
              href="//www.movistar.cl/web/movistar/atencion-al-cliente/solicitud-certificacion-administrativa/"
            >
              Inscripción de celular en Chile
            </a>
          </li>
          <li>
            <a
              href="//ww2.movistar.cl/terminos-regulaciones/multibanda-sae/"
              rel="nofollow"
            >
              Multibanda / SAE - Consulta IMEI
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/hogar/arma-tu-pack/">
              Cotizador de ofertas conjuntas hogar
            </a>
          </li>
          <li>
            <a href="//ww2.movistar.cl/movil/deglose-planes-moviles/">
              Cotizador de ofertas conjuntas móvil
            </a>
          </li>
          <li>
            <a
              href="//ww2.movistar.cl/terminos-regulaciones/documentos/Numeracion-Especial-para-Servicios-Complementarios.pdf"
            >
              Numeración especial servicios complementarios
            </a>
          </li>
          <li>
            <a
              href="//ww2.movistar.cl/terminos-regulaciones/condiciones-comerciales-y-contractuales-movil/"
            >
              Condiciones Comerciales y Contractuales
            </a>
          </li>
        </ul>
      </div>
      <div class="mvf-footer_last">
        <h3 role="button" class="js-tooglef">Más sobre Movistar</h3>
        <div class="mvf-footer_list">
          <a href="//ww2.movistar.cl/aviso-legal/">
            Términos de uso del sitio
          </a>
          <a href="//ww2.movistar.cl/sobre-movistar/quienes-somos/">
            Quiénes Somos
          </a>
          <a href="//www.fundaciontelefonica.cl/">Fundación Telefónica</a>
          <a
            href="//www.facebook.com/MovistarChile/"
            class="rrss fb"
            title="Facebook Movistar"
          >
            Facebook
          </a>
          <a
            href="//twitter.com/MovistarChile"
            class="rrss tw"
            title="Twitter Movistar"
          >
            Twitter
          </a>
          <a
            href="//ww2.movistar.cl/"
            class="logo"
            title="Ir a movistar.cl"
          ></a>
        </div>
      </div>
    </div>
  </footer>

  <!-- ./footer -->