<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <title>{{ env('APP_NAME') }}</title>
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        <link href="{{ asset('packfull/public/css/main.css') }}" rel="stylesheet">
        <script>
            const _GLOBALPAYLOAD = '{!! $payloadParam !!}';
            const _PLANB = {{ $planB }};
            const _TRANS = '{{ $key }}';
            const _PAYLOAD_VALID= {{ $payloadValid }};
            const _PLANPRICE = {{ $price }};
            const _PLANPRICEWITHDISCOUNT = {{ $price_discount }};
            const _PLANDETAIL = {!! $plan_details !!};
            const _HOMELAND = '{{ $homeland }}';
            const _PAYLOAD_DISPLAY_ERRORS = {!!  $payloadErrors !!};
            const _LEGAL_TEXT = {!! $legal_text !!};

        </script>
        
    </head>
    <body>
      
        <script type="text/x-template" id="modal-template">
            <transition name="modal">
              <div class="modal-mask">
                <div class="modal-wrapper">
                  <div class="modal-container">
      
                    <div class="modal-header">
                      <slot name="header">
                      </slot>
                    </div>
      
                    <div class="modal-body">
                      <slot name="body">
                      </slot>
                    </div>
      
                    <div class="modal-footer">
                      <slot name="footer">
                        
                      </slot>
                    </div>
                  </div>
                </div>
              </div>
            </transition>
          </script>
      {{-- @include('partials/layout/header') --}}
        <div id="app">
            <app/>
        </div>
      {{-- @include('partials/layout/footer') --}}
        <div id="mainsearch"></div>
        {{-- @<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>--}}

        <script src="{{ asset('packfull/public/js/app.js') }}{{-- ?r={{ time() }}--}}"></script>
        {{-- <script src="https://ww2.movistar.cl/base/js/base.min.js?v=70" defer></script> --}}

        
    </body>
</html>
