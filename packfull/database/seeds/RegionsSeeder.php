<?php

use App\Region;
use Illuminate\Database\Seeder;

class RegionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Region::create([ 'name' => 'Tarapacá','acronym' => 'I', 'order' => 1 ]);
        Region::create([ 'name' => 'Antofagasta','acronym' => 'II', 'order' => 2 ]);
        Region::create([ 'name' => 'Atacama','acronym' => 'III', 'order' => 3 ]);
        Region::create([ 'name' => 'Coquimbo','acronym' => 'IV', 'order' => 4 ]);
        Region::create([ 'name' => 'Valparaíso','acronym' => 'V', 'order' => 5 ]);
        Region::create([ 'name' => 'Libertador General Bernardo O’Higgins','acronym' => 'VI', 'order' => 6 ]);
        Region::create([ 'name' => 'Maule','acronym' => 'VII', 'order' => 7 ]);
        Region::create([ 'name' => 'Biobío','acronym' => 'VIII', 'order' => 8 ]);
        Region::create([ 'name' => 'La Araucanía','acronym' => 'IX', 'order' => 9 ]);
        Region::create([ 'name' => 'La Los Lagos','acronym' => 'X', 'order' => 10 ]);
        Region::create([ 'name' => 'General Carlos Ibáñez del Campo','acronym' => 'XI', 'order' => 11 ]);
        Region::create([ 'name' => 'Magallanes y Antártica Chilena','acronym' => 'XII', 'order' => 11 ]);
        Region::create([ 'name' => 'Metropolitana de Santiago','acronym' => 'XIII', 'order' => 13 ]);
        Region::create([ 'name' => 'Región de Los Ríos','acronym' => 'XIV', 'order' => 14 ]);
        Region::create([ 'name' => 'Arica y Parinacota','acronym' => 'XV', 'order' => 15 ]);
        Region::create([ 'name' => 'Ñuble','acronym' => 'XVI', 'order' => 16 ]);

    }
}
