<?php
use App\Region;
use App\Commune;
use Illuminate\Database\Seeder;

class CommunesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # region I
        $region_id = (Region::where('acronym', 'I')->first())['id'];
        
        Commune::create([ 'name' =>  'Iquique', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Pozo Almonte', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Camiña', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Colchane', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Huera', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Pica', 'region_id' => $region_id ]);

        #region II
        $region_id = (Region::where('acronym', 'II')->first())['id'];
        
        Commune::create([ 'name' =>  'Antofagasta', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Mejillones', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Sierra Gorda', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Taltal', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Calama', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Ollagüe', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Pedro de Atacama', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Tocopilla', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'María Elena', 'region_id' => $region_id ]);

        #region III
        $region_id = (Region::where('acronym', 'III')->first())['id'];

        Commune::create([ 'name' =>  'Chañaral', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Diego de Almagro', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Copiapó', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Caldera', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Tierra Amarilla', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Vallenar', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Alto del Carmen', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Freirina', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Huasco', 'region_id' => $region_id ]);

        #region IV
        $region_id = (Region::where('acronym', 'IV')->first())['id'];

        Commune::create([ 'name' =>  'Illapel', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Canela', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Los Vilos', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Salamanca', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Coquimbo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Andacollo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'La Higuera', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'La Serena', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Paihuano', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Vicuña', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Ovalle', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Combarbalá', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Monte Patria', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Río Hurtado', 'region_id' => $region_id ]);
        
        #region V
        $region_id = (Region::where('acronym', 'V')->first())['id'];
        
        Commune::create([ 'name' =>  'Los Andes', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Calle Larga', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Rinconada', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Esteban', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Quilpué', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Limache', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Olmué', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Villa Alemana', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'La Ligua', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Cabildo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Papudo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Petorca', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Zapallar', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Quillota', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Hijuelas', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'La Calera', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'La Cruz', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Nogales', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Antonio', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Algarrobo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Cartagena', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'El Quisco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Santo Domingo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Felipe', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Catemu', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Llay Llay', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Panquehue', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Putaendo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Santa María', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Valparaíso', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Casablanca', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Concón', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Juan Fernández', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Puchuncaví', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Quintero', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Viña del Mar', 'region_id' => $region_id ]);
        
        
        $region_id = (Region::where('acronym', 'VI')->first())['id'];
        
        Commune::create([ 'name' =>  'Rancagua', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Codegua', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Coinco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Coltauco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Doñihue', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Graneros', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Las Cabras', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Machalí', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Malloa', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Mostazal', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Olivar', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Peumo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Pichidegua', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Quinta de Tilcoco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Rengo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Requínoa', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Vicente de Tagua Tagua', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Pichilemu', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'La Estrella', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Litueche', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Marchigüe', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Navidad', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Paredones', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Fernando', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Chépica', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Chimbarongo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Lolol', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Nancagua', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Palmilla', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Peralillo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Placilla', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Pumanque', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Santa Cruz', 'region_id' => $region_id ]);


        # region VII
        $region_id = (Region::where('acronym', 'VII')->first())['id'];

        Commune::create([ 'name' =>  'Cauquenes', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Chanco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Pelluhue', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Curicó', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Hualañé', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Licantén', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Molina', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Rauco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Romeral', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Sagrada Familia', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Teno', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Vichuquén', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Linares', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Colbún', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Longaví', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Parral', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Retiro', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Javier', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Villa Alegre', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Yerbas Buenas', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Talca', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Constitución', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Curepto', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Empedrado', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Maule', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Pelarco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Pencahue', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Río Claro', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Clemente', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Rafael', 'region_id' => $region_id ]);

        # region VIII
        $region_id = (Region::where('acronym', 'VIII')->first())['id'];

        Commune::create([ 'name' =>  'Lebu', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Arauco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Cañete', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Contulmo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Curanilahue', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Los Álamos', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Tirúa', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Los Ángeles', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Alto Bío Bío', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Antuco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Cabrero', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Laja', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Mulchén', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Nacimiento', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Negrete', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Quilaco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Quilleco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Rosendo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Santa Bárbara', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Tucapel', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Yumbel', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Concepción', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Chiguayante', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Coronel', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Florida', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Hualpén', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Hualqui', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Lota', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Penco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Pedro de la Paz', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Santa Juana', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Talcahuano', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Tomé', 'region_id' => $region_id ]);

        # region IX
        $region_id = (Region::where('acronym', 'IX')->first())['id'];

        Commune::create([ 'name' =>  'Temuco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Carahue', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Chol Chol', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Cunco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Curarrehue', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Freire', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Galvarino', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Gorbea', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Lautaro', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Loncoche', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Melipeuco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Nueva Imperial', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Padre Las Casas', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Perquenco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Pitrufquén', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Pucón', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Saavedra', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Teodoro Schmidt', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Toltén', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Vilcún', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Villarrica', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Angol', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Collipulli', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Curacautín', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Ercilla', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Lonquimay', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Los Sauces', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Lumaco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Purén', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Renaico', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Traiguén', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Victoria', 'region_id' => $region_id ]);


        $region_id = (Region::where('acronym', 'X')->first())['id'];

        Commune::create([ 'name' =>  'Castro', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Ancud', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Chonchi', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Curaco de Vélez', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Dalcahue', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Puqueldón', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Queilén', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Quellón', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Quemchi', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Quinchao', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Puerto Montt', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Calbuco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Cochamó', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Fresia', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Frutillar', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Los Muermos', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Llanquihue', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Maullín', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Puerto Varas', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Osorno', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Puerto Octay', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Purranque', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Puyehue', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Río Negro', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Juan de la Costa', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Pablo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Chaitén', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Futaleufú', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Hualaihué', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Palena', 'region_id' => $region_id ]);


        $region_id = (Region::where('acronym', 'XI')->first())['id'];

        Commune::create([ 'name' =>  'Aysén', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Cisnes', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Guaitecas', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Cochrane', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'O Higgins', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Tortel', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Coyhaique', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Lago Verde', 'region_id' => $region_id ]);


        $region_id = (Region::where('acronym', 'XII')->first())['id'];

        Commune::create([ 'name' =>  'Cabo de Hornos', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Antártica', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Punta Arenas', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Laguna Blanca', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Río Verde', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Gregorio', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Porvenir', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Primavera', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Timaukel', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Puerto Natales', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Torres del Paine', 'region_id' => $region_id ]);



        $region_id = (Region::where('acronym', 'XIII')->first())['id'];

        Commune::create([ 'name' =>  'Colina', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Lampa', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Til Til', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Puente Alto', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Pirque', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San José de Maipo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Bernardo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Buin', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Calera de Tango', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Paine', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Melipilla', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Alhué', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Curacaví', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'María Pinto', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Pedro', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Santiago', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Cerrillos', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Cerro Navia', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Conchalí', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'El Bosque', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Estación Central', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Huechuraba', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Independencia', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'La Cisterna', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'La Florida', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'La Granja', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'La Pintana', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'La Reina', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Las Condes', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Lo Barnechea', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Lo Espejo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Lo Prado', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Macul', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Maipú', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Ñuñoa', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Pedro Aguirre Cerda', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Peñalolén', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Providencia', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Pudahuel', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Quilicura', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Quinta Normal', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Recoleta', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Renca', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Joaquín', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Miguel', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Ramón', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Vitacura', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Talagante', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'El Monte', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Isla de Maipo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Padre Hurtado', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Peñaflor', 'region_id' => $region_id ]);


        $region_id = (Region::where('acronym', 'XIV')->first())['id'];

        Commune::create([ 'name' =>  'La Unión', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Futrono', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Lago Ranco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Río Bueno', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Valdivia', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Corral', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Lanco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Los Lagos', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Máfil', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Mariquina', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Paillaco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Panguipulli', 'region_id' => $region_id ]);


        $region_id = (Region::where('acronym', 'XV')->first())['id'];


        Commune::create([ 'name' =>  'Arica', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Camarones', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Putre', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'General Lagos', 'region_id' => $region_id ]);


        $region_id = (Region::where('acronym', 'XVI')->first())['id'];

        Commune::create([ 'name' =>  'Bulnes', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Chillán', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Chillán Viejo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'El Carmen', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Pemuco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Pinto', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Quillón', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Ignacio', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Yungay', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Quirihue', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Cobquecura', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Coelemu', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Ninhue', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Portezuelo', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Ránquil', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Trehuaco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Carlos', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Coihueco', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'Ñiquén', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Fabián', 'region_id' => $region_id ]);
        Commune::create([ 'name' =>  'San Nicolás', 'region_id' => $region_id ]);

    }
}
