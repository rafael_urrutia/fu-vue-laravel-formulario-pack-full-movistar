<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'email' => 'packfull.movistar@movistar.cl',
            'password' => '$2y$10$mRvSuVZliihvQwES67aIlebiO7CoEZFYPHXPIKg8ja4KGeSVWeKBC' # use bcrypt('password')
        ]);

    }
}
