<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('portability_phone')->nullable();
            $table->char('is_phone_new', 1);
            $table->integer('portability_phone_plan_b')->nullable();
            $table->char('is_phone_new_plan_b', 1)->nullable();
            $table->string('plan_name', 255)->nullable();
            $table->integer('plan_price');
            $table->integer('plan_price_on_discount');
            $table->string('plan_details',500);
            $table->string('run', 12);
            $table->string('name', 255);
            $table->string('lastname', 255);
            $table->integer('phone_contact');
            $table->string('email', 255);
            $table->string('street', 255);
            $table->integer('number_street');
            $table->string('apartment_number', 10)->nullable();
            $table->string('aditional_data', 500)->nullable();
            $table->char('use_current_street', 1);
            $table->longText('form_information');
            $table->unsignedInteger('commune_id')->nullable();
            $table->foreign('commune_id')
                    ->references('id')
                    ->on('communes')
                    ->onDelete('CASCADE');
            $table->unsignedInteger('other_commune_id')->nullable();
            $table->foreign('other_commune_id')
                    ->references('id')
                    ->on('communes')
                    ->onDelete('CASCADE');
            $table->string('other_street')->nullable();
            $table->integer('other_number_street')->nullable();
            $table->string('other_apartment_number', 10)->nullable();
            $table->longText('other_aditional_data')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('requests');
        Schema::enableForeignKeyConstraints();
        
    }
}
