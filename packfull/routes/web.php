<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', [HomeController::class , 'index']);
// Route::get('/{planName}', [HomeController::class, 'index']);
// Route::get('/{planName}/{planType}', [HomeController::class, 'index']);
//Auth::routes();

Route::match(['GET', 'POST'], '/', [ HomeController::class, 'index' ]);

Route::group(['prefix' => 'acl/packfullform_panel'], function() {
  Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
  ]);

  Route::get('/dashboard', 'Backend\DashboardController@index')->name('dashboard_admin')->middleware('auth');

  Route::get('/dashboard', 'Backend\DashboardController@index')->name('dashboard_admin')->middleware('auth');
  Route::post('/dashboard/generateFileReport', 'Backend\DashboardController@generateFileReport')->name('generate_file_report')->middleware('auth');
  Route::get('/dashboard/file/download/{fileName}', 'Backend\DashboardController@download')->name('download_file_report')->middleware('auth');
  Route::post('/dashboard/file/checkDownload/', 'Backend\DashboardController@checkDownload')->name('download_check_file_report')->middleware('auth');



});



Route::get('/home', 'HomeController@index')->name('home');
