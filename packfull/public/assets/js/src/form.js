const step1 = document.querySelector("#step-1");
const step2 = document.querySelector("#step-2");
const continueBtn = document.querySelector("#step-1 .button-container button");
const backBtn = document.querySelectorAll(
  "#step-2 .form-header .banner .arrow-back"
);

// back to step 1
backBtn.forEach((btn) => {
  btn.addEventListener("click", function () {
    step1.classList.add("active");
    step2.classList.remove("active");
  });
});

// ----Label animation----
const floatLabel = (() => {
  // Add active class and placeholder
  const handleFocus = (e) => {
    const target = e.target;
    target.parentNode.classList.add("active");
    target.setAttribute("placeholder", target.getAttribute("data-placeholder"));
  };

  // Remove active class and placeholder
  const handleBlur = (e) => {
    const target = e.target;
    if (!target.value) {
      target.parentNode.classList.remove("active");
    }
    target.removeAttribute("placeholder");
  };

  // register events
  const bindEvents = (element) => {
    const floatField = element.querySelector("input, select, textarea");
    floatField.addEventListener("focus", handleFocus);
    floatField.addEventListener("blur", handleBlur);
  };

  // get DOM elements
  const init = () => {
    const floatContainers = document.querySelectorAll(".form-group .input");
    floatContainers.forEach((elem) => {
      if (elem.querySelector("input, select, textarea").value) {
        elem.classList.add("active");
      }

      bindEvents(elem);
    });
  };
  return { init: init };
})();

floatLabel.init();

function soloNumeros(e){
    var key = window.Event ? e.which : e.keyCode
	return (key >= 48 && key <= 57 || key == 107 || key == 75)
}

const csrf_token = document.querySelector('meta[name="csrf-token"]').content;

continueBtn.addEventListener("click", function () {
    let divs = document.querySelectorAll('.form-group')
    for (var i = 0; i < divs.length; ++i) {
        divs[i].classList.remove('error');
    }

    let erros_span = document.querySelectorAll(`.field-error`)
    for (var i = 0; i < divs.erros_span; ++i) {
        erros_span[i].innerHTML = ''
    }

    let csrf_token = document.querySelector('[name="_token"]').value;
    let name = document.querySelector('#name').value;
    let lastname = document.querySelector('#lastname').value;
    let rut = document.querySelector('#rut').value;
    let phone = document.querySelector('#phone').value;
    let email = document.querySelector('#email').value;

    data = {
        name,
        lastname,
        rut,
        phone,
        email,
        csrf_token
    }
    global.loading();
    fetch(URL_STEP1, {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': csrf_token,
                'Content-Type': 'application/json',
                "Accept": "application/json",
            },
            body: JSON.stringify(data)
        })
        .then(function (response) {
            return response.json();
        })
        .then(function (res) {
            global.loading(true);
            if (res.errors ) {
                throw res
            }
            if (!res.success) {
                return;
            }
            step1.classList.remove("active");
            step2.classList.add("active");
        })
        .catch(function (err) {
            if (err.errors) {
                for (field in err.errors) {
                    document.querySelector(`#${field}`).closest('.form-group').classList.add('error');
                    document.querySelector(`#error_${field}`).innerHTML = `${err.errors[field][0]}`
                }
            }
        });
});

document.querySelector('#region_id').addEventListener('change', function () {
    let region_id = document.querySelector('#region_id').value
    let csrf_token = document.querySelector('[name="_token"]').value;

    let data = {
        region_id,
        csrf_token
    }
    global.loading();
    fetch(URL_COMMUNES, {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': csrf_token,
                'Content-Type': 'application/json',
                "Accept": "application/json",
            },
            body: JSON.stringify(data)
        })
        .then(function (response) {
            return response.json();
        })
        .then(function (res) {
            global.loading(true);
            if (res.errors ) {
                throw res
            }
            if (!res.success) {
                return;
            }
            let html_node = [];
            for (let i in res.data) {
                let item = res.data[i];
                let html = `<option value="${item.id}">${item.name}</option>`;
                html_node.push(html);
            }
            document.querySelector('#commune_id').innerHTML = html_node.join('')
        })
        .catch(function (err) {
            if (err.errors) {
                for (field in err.errors) {
                    document.querySelector(`#${field}`).closest('.form-group').classList.add('error');
                    document.querySelector(`#error_${field}`).innerHTML = `${err.errors[field][0]}`
                }
            }
        });
});

document.querySelector('#sendForm').addEventListener('click', function () {
    let region_id = document.querySelector('#region_id').value
    let commune_id = document.querySelector('#commune_id').value
    let street = document.querySelector('#street').value
    let number = document.querySelector('#number').value
    let apartment = document.querySelector('#apartment').value
    let additional_info = document.querySelector('#additional_info').value
    let company_id = document.querySelector('#company_id').value
    let plan_id = document.querySelector('#plan_id').value
    let type_form = document.querySelector('#type_form').value
    let request_type = document.querySelector('#request_type').value
    let csrf_token = document.querySelector('[name="_token"]').value;


    let data = {
        region_id,
        commune_id,
        street,
        number,
        apartment,
        company_id,
        plan_id,
        type_form,
        additional_info,
        request_type,
        csrf_token
    }
    global.loading();
    fetch(URL_STORE, {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': csrf_token,
                'Content-Type': 'application/json',
                "Accept": "application/json",
            },
            body: JSON.stringify(data)
        })
        .then(function (response) {
            return response.json();
        })
        .then(function (res) {
            global.loading(true);
            if (res.errors ) {
                throw res
            }
            if (!res.success) {
                return;
            }
            window.location.href = URL_SUCCESS;
        }).catch(function (err){
            if (err.errors) {
                for (field in err.errors) {
                    document.querySelector(`#${field}`).closest('.form-group').classList.add('error');
                    document.querySelector(`#error_${field}`).innerHTML = `${err.errors[field][0]}`
                }
            }
        })
})

const global = {
    loading: function (hide = false) {


        // document.querySelector('.button-container').classList.add("loading");
        // document.querySelector('.button-container').setAttribute('disabled', 'disabled')

        let buttons= document.querySelectorAll(".button-container");
        buttons.forEach((elem) => {
            elem.classList.add("loading");
            elem.setAttribute('disabled', 'disabled')
        });

        if (hide) {
            buttons.forEach((elem) => {
                elem.classList.remove("loading");
                elem.removeAttribute('disabled', 'disabled')
            });
        }

        
    },


}