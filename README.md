# Pack Full

Aplicación que permite guardar registros de las solicitudes enviadas a través del formulario integrado. Este formulario recoge valores a través del método GET y asi es enviado a un registro en la base de datos de la aplicación.


## URL del proyecto

QA:

```
https://formulario-movistar-con-todo.fusionadns.cl
```

produccion:
```
https://formulario-movistar-con-todo.mvch.cl/
```

## Credenciales del backoffice

https://formulario-movistar-con-todo.fusionadns.cl/acl/packfullform_panel/login

usuario: packfull.movistar@movistar.cl
clave: Y3H6KVAUPM.

URLs de origen de obtención de datos, forma (json, hash, etc), almacenamiento y envío (correo, api, etc) o exportación de estos
 
La aplicación cuenta con una api con una api interna dentro de la url bajo /api en la cual es sacada directamente desde la base de datos de la aplicación.
 
Esta api es posible consumir debido a un token que se genera de manera aleatoria al ingresar al sitio la cual tiene una duración de 20 minutos una vez creada.
 
El json encodeado debe ser de la siguiente manera:

```

 {
     "type":"duo",
     "price":22222,
     "plan_details": [
        "Ejemplo detalle uno",
        "Ejemplo detalle dos",
        "Ejemplo detalle tres"
      ],
      "discount":11111
}

```

Es preferible que al momento de encriptar en base64 dejar solo una linea asi sera mas pequeño el payload

```
   {"type":"duo","price":22222,"plan_details":["Ejemplo detalle uno","Ejemplo detalle dos","Ejemplo detalle tres"],"discount":11111}

```
Los datos del json entregado son los siguientes:



| Nombre       | Tipo          | Requerido | Descripcion                                                                                                                                                |
|--------------|---------------|-----------|------------------------------------------------------------------------------------------------------------------------------------------------------------|
| type         | string        | no        | tipo de plan a desplegar, puede ser single o duo (defecto single)                                                                                          |
| price        | number        | si        | valor del plan                                                                                                                                             |
| plan_details | Array<string> | si        | Listado de los detalles del plan. Seran desplegados en la lista de detalles en la parte derecha del formulario. Deber ser mínimo dos detalles, máximo tres |
| discount     | number        | no        | Precio resultante con el descuento. Este valor no debe ser mayor que el precio original                                                                    |


En caso que cualquier parámetro señalado no cumpla lo especificado, el formulario no mostrará el botón continuar, si el json entregado es invalido o no esta en el formato señalado, el botón continuar tampoco será desplegado

un ejemplo de url listo seria lo siguiente:


```
https://formulario-movistar-con-todo.fusionadns.cl/?payload=eyJ0eXBlIjoiZHVvIiwicHJpY2UiOiAzMzE3NCwicGxhbl9kZXRhaWxzIjogWyJQbGFuIGhvZ2FyIHRyacOzIGZpYnJhIMOzcHRpY2EgODAwIE1lZ2FzIiwgIlBsYW4gNjAwIGdpZ2FzIiwgIlBsYW4gNjAwIGdpZ2FzIl0gLCAiZGlzY291bnQiOiAxMjM0NH0=
```

Soporta tambien enviar el payload por post a la url https://formulario-movistar-con-todo.fusionadns.cl , se puede por usar el texto json como el encodeado base64

Un ejemplo usando javascript podria ser 
```
...
<header>

      <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
</header>
<body>

....

<script>

function iframeform(url)
{
    var object = this;
    object.time = new Date().getTime();
    object.form = $('<form action="'+url+'" target="iframe'+object.time+'" method="post" style="display:none;" id="form'+object.time+'" name="form'+object.time+'"></form>');

    object.addParameter = function(parameter,value)
    {
        $("<input type='hidden' />")
         .attr("name", parameter)
         .attr("value", value)
         .appendTo(object.form);
    }

    object.send = function()
    {
        var iframe = $('<iframe data-time="'+object.time+'" style="display:none;" id="iframe'+object.time+'"></iframe>');
        $( "body" ).append(iframe); 
        $( "body" ).append(object.form);
        object.form.submit();
        iframe.load(function(){  $('#form'+$(this).data('time')).remove();  $(this).remove();   });
    }
}

var dummy = new iframeform('http://localhost');
dummy.addParameter('payload','eyJ0eXBlIjoiZHVvIiwicHJpY2UiOiAzMzE3NCwicGxhbl9kZXRhaWxzIjogWyJQbGFuIGhvZ2FyIHRyacOzIGZpYnJhIMOzcHRpY2EgODAwIE1lZ2FzIiwgIlBsYW4gNjAwIGdpZ2FzIiwgIlBsYW4gNjAwIGdpZ2FzIl0gLCAiZGlzY291bnQiOiAxMjM0NCwibGVnYWxfdGV4dCI6ICJCZW5lZmljaW8gTW92aXN0YXIgQ29uIFRvZG94eHgifQ==');
dummy.send();

o directo con json

var dummy = new iframeform('http://localhost');
dummy.addParameter('payload','{"type":"duo","price": 33174,"plan_details": ["Plan hogar trió fibra óptica 800 Megas", "Plan 600 gigas", "Plan 600 gigas"] , "discount": 12344,"legal_text": "Beneficio Movistar Con Todoxxx"}');
dummy.send();

</script>

```

 
## Tecnologías back y front usadas (ddbb, lenguajes, frameworks, librerías, etc.)
 
* PHP
* Framework Laravel
* VUEJS
* MYSQL
 
Configuraciones

## Docker

La aplicacion tiene incluido el contenedor docker el cual carga en la siguiente direccion:

* http://localhost/

La persistencia de mysql esta guardada en el directorio /docker/mysql.

Tambien logs de apache(/docker/logs), etc

La configuracion de apache en /docker/webserver/Dockerfile

## Archivo .env se encuentras las configuraciones del sitio

Las rutas de las aplicaciones se encuentran configuradas en los siguientes archivos.
* /packfull/routes/api.php (direcciones de la apis)
* /packfull/routes/web.php (direcciones del form)

Para desarrollar con vue, es que usar el siguiente comando dentro del contenedor docker

```
npm run watch-poll
```
En caso que no funcione el comando anterior, favor reconstruir el proyecto con el siguiente comando:

```
npm rebuild
```

Para compilar vue en modo produccion, ejecutar el siguiente comando


```
npm run production
```

Asegurarse que el archivo /packfull/.env en la variable APP_ENV este con el valor *production*

para cargar la base de datos es que usar el siguiente comando

```
php artisan migrate:fresh --seed
```

> :warning: ** El comando anterior no debe ejecutarse en produccion, ya que borrara la base de datos y recargara nuevamente desde cero

